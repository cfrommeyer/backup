# Backup

Uses rsync to backup one or several directories to a target.

## Usage

The script takes a number of options:

    backup [-d|--list-debian-packages] [-e|--exclude-file file] [-l|--log-file logfile] [--no-log] [-m|--rotate-monthly]"
           [-p|--mount-point mountpoint] [-n|--dry-run] [--help] [[SOURCE...] TARGET]"

### Arguments

| -d, --list-debian-packages | List packages installed via dpkg to the log |
| -e, --exclude-file | Allows to specify a file with rsync exclude patterns |
| -l, --log-file | Allows to specify a log file for the backup run |
| --no-log | Switches off logging to a file and forces log to the console |
| -m, --rotate-monthly | Instead of using the full date (yyyyMMdd) to qualify the backups, only use the day part. Results in keeping  a month of backups |
| -p, --mount-point | Specify a mount point that should be mounted before backing up data. Usually used to attach an external drive hosting the target. |
| -n, --dry-run | Pass the dry-run parameter to rsync to not really perform the operation. |
| --help | display this help |


The backup script can be called with an optional non option argument indicating the TARGET direcetory for the backup.
If the command is called with at least two non option arguments the last argument is taken to be the TARGET and all other
non option arguments are considered to be SOURCE directories.

### Configuration Files

By default `backup´ will assume a mountable device at `/media/autobackup` and will backup the root directory to the
`/root` folder on the ounted device. Those and other options can be set in a config file located at `/etc/default/backup`
for all users and in a user specific file located under `~/.config/backuprc`. The user specific file will take precedence
if a configuration key is located in both.

## Development

### CODE

This script is composed from several individual files. All files contributing to the script reside in the `src/bash` folder.
Scripts are numbered in the order they get concatenated.

### TESTS

Tests should go into the `test` folder.

### BUILD

to create scripts call

 $ make

