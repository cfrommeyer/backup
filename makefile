VERSION = 0.0.1

BASH_SRC_DIR = src/bash/

backup: $(wildcard $(BASH_SRC_DIR)*.sh)
	cat $(BASH_SRC_DIR)*.sh > backup
	chmod a+x backup

clean:
	rm backup

