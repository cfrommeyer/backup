####################### Functions used for backup script #######################

append_slash () {
  local __VAR_NAME=$1
  local VALUE=$(eval "echo $""$__VAR_NAME")
  if [ "${VALUE:${#VALUE}-1:1}" != "/" ]; then
    eval "$__VAR_NAME=$VALUE/"
  fi
}

mount_target () {
  if [ "$1" ]; then
    if [[ -d "$1" ]]; then
      debug "Mounting '$1'"
      local MOUNTED=$($MOUNT | $FGREP "$1");
      if [ -z "$MOUNTED" ]; then
        run_and_log $MOUNT "$1"
        MOUNTED=$($MOUNT | $FGREP "$1");
      fi
      if [ -z "$MOUNTED" ]; then
        error "Couldn't mount '$1'"
        exit 10
      fi
    else
      error "Mount point '$1' does not exist, or is not a directory"
      exit 11
    fi
  else
    debug "Mounting of target device not active"
  fi
}

rotation_date () {
  if [ -z "$MONTHROTATE" ]; then
    $DATE +%y%m%d
  else
    $DATE +%d
  fi
}

create_backup () {
  debug "Starting rsync..."
  if [[ "$EXCLUDE_FILE" ]]; then
    RSYNCCONF+=("--exclude-from $EXCLUDE_FILE")
  fi
  if [[ "$DRY_RUN" ]]; then
    RSYNCCONF+=("--dry-run")
  fi
  RSYNCCONF+=("--link-dest=$TARGET$LAST")
  debug "Checking target directory >$TARGET<"
  if [[ ! -d $TARGET ]]; then
    warn "Target directory >$TARGET< does not exist. Trying to create it..."
    if [[ ! "$DRY_RUN" ]]; then
      mkdir -p $TARGET
      if [[ ! $? ]]; then
        error "... failed to create directory"
	exit 31
      fi
      warn "done."
    else
      debug "Skipping because of dry run"
    fi
  fi
  for SOURCE in "${SOURCES[@]}"; do
    run_and_log "$RSYNC --archive --verbose --one-file-system ${RSYNCCONF[@]} \"$SOURCE\" $TARGET$TODAY" || exit $?
  done
  debug "Backup completed"
}

################################################################################
