################################# Main Program #################################

parse_options "$@" || exit $?
debug $PARSED_OPTIONS
eval process_options $PARSED_OPTIONS || exit $?
unset PARSED_OPTIONS

info "Starting at: $($DATE)"
append_slash TARGET

if [ "$LISTPACKAGES" ]; then
  run_and_log "$DPKG --get-selections | $AWK '!/deinstall|purge|hold/' | $CUT -f1 | $TR '\n' ' ' | $SED '\$a\'" || exit $?
fi

mount_target "$MOUNT_POINT" || exit $?
TODAY=$(rotation_date)

create_backup

if [[ -z "$DRY_RUN" ]]; then
  run_and_log "$LN -nsf $TARGET$TODAY $TARGET$LAST" || exit $?
fi

info "Finished at: $($DATE)"
