################################### Defaults ###################################

SOURCES=(/ )
TARGET="/media/autobackup/root"
MOUNT_POINT="/media/autobackup"
EXCLUDE_FILE=config/root-excludes

RSYNCCONF=()

LAST="last"
LOG=log/$0.log

if [[ -f "/etc/default/backup" ]]; then
  . /etc/default/backup
fi

if [[ -f "${HOME}/.config/backuprc" ]]; then
  . ${HOME}/.config/backuprc
fi

################################################################################
