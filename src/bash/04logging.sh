################################# LOG  TOOLING #################################

LOG_BLUE="\e[34m"
LOG_YELLOW="\e[93m"
LOG_RED="\e[91m"
LOG_RESET="\e[0m"


timestamp () {
  $DATE +%FT%T.%N
}

debug () {
  if [ "$DEBUG" ]; then
    if [ "$LOG" ]; then
      echo "$(timestamp) - DEBUG --- $@" >> $LOG
    else
      echo -e "${LOG_BLUE}$@${LOG_RESET}"
    fi
  fi
}

warn () {
  if [ "$LOG" ]; then 
    echo "$(timestamp) - WARN  --- $@" >> $LOG
  fi
  echo -e "${LOG_YELLOW}$@${LOG_RESET}" >&2
}

error () {
  if [ "$LOG" ]; then 
    echo "$(timestamp) - ERROR --- $@" >> $LOG
  fi
  echo -e "${LOG_RED}$@${LOG_RESET}" >&2
}

info () {
  if [ "$LOG" ]; then
    echo "$(timestamp) - INFO  --- $@" >> $LOG
  else
    echo -e "$@"
  fi
}

roll_log () {
  if [ "$LOG" ]; then
    savelog -t -c 7 -j $LOG
  fi
}

run_and_log () {
  info "Runnig: $@"
  if [ "$LOG" ]; then
    echo "--------------------" >> $LOG
    eval $@ >> $LOG 2>&1
    echo "--------------------" >> $LOG
  else
    eval $@
  fi
  if [ $? -ne 0 ]; then
    error "Command failed to execute"
    exit 20
  fi
}

create_logdir() {
  if [[ $LOG ]]; then
    mkdir -p $(dirname $LOG)
    if [[ ! $? ]]; then
      error "Couldn't create logdir"
      exit 21
    fi
  fi
}

################################################################################
