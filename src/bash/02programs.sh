######################### Programms used in the script #########################

MOUNT="/bin/mount"
FGREP="/bin/fgrep"
LN="/bin/ln"
ECHO="/bin/echo"
DATE="/bin/date"
RM="/bin/rm"
DPKG="/usr/bin/dpkg"
AWK="/usr/bin/awk"
CUT="/usr/bin/cut"
TR="/usr/bin/tr"
RSYNC="/usr/bin/rsync"
SED="/usr/bin/sed"

################################################################################
