###################### Functions for options parsing  ##########################

usage () {
  echo " ____                            ___           ____                     __                      ____                               __      "
  echo "/\  _\`\   __                    /\_ \         /\  _\`\                  /\ \                    /\  _\`\                  __        /\ \__   "
  echo "\ \,\L\_\/\_\    ___ ___   _____\//\ \      __\ \ \L\ \     __      ___\ \ \/'\   __  __  _____\ \,\L\_\    ___   _ __ /\_\  _____\ \ ,_\  "
  echo " \/_\__ \\\\/\ \ /' __\` __\`\/\ '__\`\\\\ \ \   /'__\`\ \  _ <'  /'__\`\   /'___\ \ , <  /\ \/\ \/\ '__\`\/_\__ \   /'___\/\\\`'__\/\ \/\ '__\`\ \ \/  "
  echo "   /\ \L\ \ \ \/\ \/\ \/\ \ \ \L\ \\\\_\ \_/\  __/\ \ \L\ \/\ \L\.\_/\ \__/\ \ \\\\\`\\\\ \ \_\ \ \ \L\ \/\ \L\ \/\ \__/\ \ \/ \ \ \ \ \L\ \ \ \_ "
  echo "   \ \`\____\ \_\ \_\ \_\ \_\ \ ,__//\____\ \____\\\\ \____/\ \__/.\_\ \____\\\\ \_\ \_\ \____/\ \ ,__/\ \`\____\ \____\\\\ \_\  \ \_\ \ ,__/\ \__\\"
  echo "    \/_____/\/_/\/_/\/_/\/_/\ \ \/ \/____/\/____/ \/___/  \/__/\/_/\/____/ \/_/\/_/\/___/  \ \ \/  \/_____/\/____/ \/_/   \/_/\ \ \/  \/__/"
  echo "                             \ \_\                                                          \ \_\                              \ \_\       "
  echo "                              \/_/                                                           \/_/   by Christian Frommeyer      \/_/       "
  echo
  echo "USAGE:"
  echo "  backup [-d|--list-debian-packages] [-e|--exclude-file file] [-l|--log-file logfile] [--no-log] [-m|--rotate-monthly]"
  echo "         [-p|--mount-point mountpoint] [-n|--dry-run] [--help] [[SOURCE...] TARGET]"
  echo
  echo "Copies files from the sources to the target directory."
  echo
  echo "Arguments:"
  echo "  -d, --list-debian-packages   List packages installed via dpkg to the log"
  echo "  -e, --exclude-file           Allows to specify a file with rsync exclude patterns"
  echo "  -l, --log-file               Allows to specify a log file for the backup run"
  echo "  --no-log                     Switches off logging to a file and forces log to the console"
  echo "  -m, --rotate-monthly         Instead of using the full date (yyyyMMdd) to qualify the backups, only use the day part. Results in keeping"
  echo "                               a month of backups"
  echo "  -p, --mount-point            Specify a mount point that should be mounted before backing up data. Usually used to attach an external"
  echo "                               drive hosting the target."
  echo "  -n, --dry-run                Pass the dry-run parameter to rsync to not really perform the operation."
  echo "  --help                       display this help"
  echo
  echo "The backup script can be called with an optional non option argument indicating the TARGET direcetory for the backup. If the command is"
  echo "called with at least two non option arguments the last argument is taken to be the TARGET and all other non option arguments are considered"
  echo "to be SOURCE directories."
}

parse_options () {
  PARSED_OPTIONS=$(getopt -o 'de:l:mnp:' --long 'list-debian-packages,exclude-file:,log-file:,no-log,rotate-monthly,mount-point:,dry-run,help' -n 'backup' -- "$@")
  local EXIT_CODE=$?
  if [ $EXIT_CODE -ne 0 ]; then
    error "GetOpt termiated with exit code: $EXIT_CODE"
    usage
    exit 1
  fi
}

process_options () {
  while true; do
    case "$1" in
      '-d'|'--list-debian-packages')
        debug "Do list Debian Packages"
        LISTPACKAGES=listdebianpackages
        shift
        ;;
      '-e'|'--exclude-file')
        debug "Use exclude patterns from file: $2"
        EXCLUDE_FILE=$2
        shift 2
        ;;
      '-l'|'--log-file')
        debug "Log progress to file: $2"
        LOG=$2
        shift 2
        ;;
      '--no-log')
        debug "Switch off logging"
        unset LOG
        shift
        ;;
      '-m'|'--rotate-monthly')
        debug "Rotate backups within a month only"
        MONTHROTATE=monthrotate
        shift
        ;;
      '-p'|'--mount-point')
        debug "Mounting backup device at: $2"
        MOUNT_POINT=$2
        shift 2
        ;;
      '-n'|'--dry-run')
        debug "Performing a dry run"
        DRY_RUN=dryrun
        shift
        ;;
      '--help')
        usage
        exit 0
        ;;
      '--')
        shift
        break
        ;;
      *)
        error "Error parsing options"
        usage
        exit 1
        ;;
    esac
  done

  roll_log
  if [ -n "$2" ]; then
    SOURCES=()
    while [ -n "$2" ]; do
      SOURCES+=($1)
      shift
    done
  fi
  if [ -n "$1" ]; then
    TARGET=$1
    shift
  fi
  info "Backing up from '${SOURCES[@]}' .. to '$TARGET'"
}

################################################################################
